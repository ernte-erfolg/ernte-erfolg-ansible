#!/bin/bash

INVENTORY=$1
NEW_HOST=$2
if [ -z "$INVENTORY" ]; then
  echo "Usage: $0 [production | testing] <new_host=true>"
fi

if [ "$NEW_HOST" == "new_host=true" ]; then
  ansible-playbook -i $INVENTORY.ini -u root ernte_erfolg.yml -t "admins, secure"
  ansible-playbook -i $INVENTORY.ini -u $USER -b -K ernte_erfolg.yml
else 
  ansible-playbook -i $INVENTORY.ini -u $USER -b -K ernte_erfolg.yml --skip-tags firstrun
fi
