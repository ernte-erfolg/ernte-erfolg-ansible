#!/bin/bash

export EE_DB_PASSWORD="$(cat /mnt/securedata/ee_db_password)"
export MATOMO_DB_ROOT_PW="$(cat /mnt/securedata/matomo_db_root_password)"

if [ "$(docker ps -q -f name=ee_production_database_1)" ]; then
  if [ "$(docker volume ls -q -f name=ee_production_dbdata)" ]; then
    docker exec -e PGPASSWORD=$EE_DB_PASSWORD ee_production_database_1 sh -c 'exec pg_dumpall -U ernte --clean' > /var/data/backup/ee_production_database.dump
  fi
fi

if [ "$(docker ps -q -f name=ee_production_matomodb_1)" ]; then
  if [ "$(docker volume ls -q -f name=ee_production_analyticsdata)" ]; then
    docker exec -e MYSQL_ROOT_PASSWORD=$MATOMO_DB_ROOT_PW ee_production_matomodb_1 sh -c 'exec mysqldump --all-databases -uroot -p"$MYSQL_ROOT_PASSWORD"' > /var/data/backup/ee_production_analyticsdata.dump
  fi
fi

if [ "$(docker ps -q -f name=ee_production_matomo_1)" ]; then
  if [ "$(docker volume ls -q -f name=ee_production_matomo)" ]; then
    docker stop ee_production_matomo_1
    docker run --rm --volumes-from ee_production_matomo_1 -v /var/data/backup:/backup matomo:3-apache sh -c 'cd /var/www/html && tar -cvjf /backup/ee_production_matomo.tar.bz2 .'
    docker start ee_production_matomo_1
  fi
fi

/usr/bin/restic -r /mnt/backup -p /mnt/securedata/restic --verbose backup /var/data/backup/

rm /var/data/backup/*
